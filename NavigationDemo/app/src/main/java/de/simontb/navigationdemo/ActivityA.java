package de.simontb.navigationdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class ActivityA extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        Button startB = findViewById(R.id.button_b);
        Button startD = findViewById(R.id.button_d);
        startB.setOnClickListener(view -> startActivity(new Intent(this, ActivityB.class)));
        startD.setOnClickListener(view -> startActivity(new Intent(this, ActivityD.class)));
    }

}
