package de.simontb.navigationdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class ActivityD extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d);
        Button startB = findViewById(R.id.button_b);
        Button startC = findViewById(R.id.button_c);
        startB.setOnClickListener(view -> startActivity(new Intent(this, ActivityB.class)));
        startC.setOnClickListener(view -> startActivity(new Intent(this, ActivityC.class)));
    }
}
