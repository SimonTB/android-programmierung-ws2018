package de.simontb.navigationdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class ActivityB extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        Button startC = findViewById(R.id.button_c);
        Button startD = findViewById(R.id.button_d);
        startC.setOnClickListener(view -> startActivity(new Intent(this, ActivityC.class)));
        startD.setOnClickListener(view -> startActivity(new Intent(this, ActivityD.class)));
    }
}
