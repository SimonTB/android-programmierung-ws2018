package de.simontb.navigationdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class ActivityC extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c);
        Button startC = findViewById(R.id.button_c);
        Button startD = findViewById(R.id.button_d);
        startC.setOnClickListener(view -> startActivity(new Intent(this, ActivityC.class)));
        startD.setOnClickListener(view -> startActivity(new Intent(this, ActivityD.class)));

        EditText input = findViewById(R.id.editText);
        Button shareButton = findViewById(R.id.button_share);
        shareButton.setOnClickListener(view -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, input.getText().toString());
            startActivity(sendIntent);
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
