package de.simontb.helloworld2;

import org.junit.Test;

import static org.junit.Assert.*;

public class NameValidatorTest {

    private final NameValidator validator = new NameValidator();

    @Test
    public void testWithValidName() {
        String name = "Max";

        boolean result = validator.validate(name);

        assertTrue(result);
    }

    @Test
    public void testNullIsInvalidName() {
        assertFalse(validator.validate(null));
    }

    @Test
    public void testEmptyStringIsInvalidName() {
        assertFalse(validator.validate(" "));
    }

    @Test
    public void testWhitespaceNotAllowed() {
        String name = "Max Mustermann";

        assertFalse(validator.validate(name));
    }
}