package de.simontb.helloworld2;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.app.Activity.RESULT_OK;
import static android.app.Instrumentation.*;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.contrib.ActivityResultMatchers.hasResultCode;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NameInputActivityTest {

    @Rule
    public ActivityTestRule<NameInputActivity> rule =
            new ActivityTestRule<>(NameInputActivity.class);

    @Test
    public void testEnterName() {
        onView(withId(R.id.nameInput))
                .perform(typeText("Johannes"));
        onView(withId(R.id.okButton))
                .perform(click());

        ActivityResult result = rule.getActivityResult();
        assertThat(result, hasResultCode(RESULT_OK));
        assertThat(result.getResultData(), hasExtra("extra_name", "Johannes"));
    }

}