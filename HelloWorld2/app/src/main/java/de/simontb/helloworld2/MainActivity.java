package de.simontb.helloworld2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_NAME_INPUT = 1;
    private TextView greetingTextView;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        greetingTextView = findViewById(R.id.greetingTextView);
        if (settings.getBoolean("pref_should_remember_name", true)
                && preferences.contains("pref_name")) {
            String name = preferences.getString("pref_name", null);
            String greeting = "Hello " + name;
            greetingTextView.setText(greeting);
        }
        Button settingsButton = findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
        Button newNameButton = findViewById(R.id.nameInputButton);
        newNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nameInputIntent =
                        new Intent(MainActivity.this,
                                NameInputActivity.class);
                startActivityForResult(nameInputIntent, REQUEST_CODE_NAME_INPUT);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_NAME_INPUT:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra("extra_name");
                    String greeting = "Hello " + name;
                    greetingTextView.setText(greeting);
                    preferences.edit().putString("pref_name", name).apply();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
