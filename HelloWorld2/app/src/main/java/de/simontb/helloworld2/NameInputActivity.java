package de.simontb.helloworld2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NameInputActivity extends AppCompatActivity {

    private final NameValidator validator = new NameValidator();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_input);
        final EditText nameInput = findViewById(R.id.nameInput);
        Button okButton = findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = String.valueOf(nameInput.getText());
                if (!validator.validate(name)) {
                    Toast.makeText(NameInputActivity.this, "Enter a valid name", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra("extra_name", name);
                NameInputActivity.this.setResult(RESULT_OK, resultIntent);
                NameInputActivity.this.finish();
            }
        });
    }
}
