package de.simontb.helloworld2;

public class NameValidator {

    public boolean validate(String name) {
        return null != name && name.trim().length() > 1 && !name.contains(" ");
    }

}
