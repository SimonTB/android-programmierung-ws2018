package de.simontb.badthreading;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private MyTask myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        try {
//            BufferedInputStream in = new BufferedInputStream(new URL("https://www.baeldung.com/java-download-file").openStream());
//            FileOutputStream fileOutputStream = new FileOutputStream("myfile");
//            byte dataBuffer[] = new byte[1024];
//            int bytesRead;
//            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
//                fileOutputStream.write(dataBuffer, 0, bytesRead);
//            }
//        } catch (IOException e) {
//        }


        textView = findViewById(R.id.text1);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(500L);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                textView.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        textView.setText("Hello from new Thread");
//                    }
//                });
//
//            }
//        }).start();

        myTask = new MyTask();
        myTask.setTextView(textView);
        myTask.execute();
        textView.setText("Hello from main Thread");
    }

    @Override
    protected void onDestroy() {
        myTask.setTextView(null);
        super.onDestroy();
    }

    public static class MyTask extends AsyncTask<Void, Void, String> {

        private TextView textView; //statt View(mit Context) ein Interface definieren

        @Override
        protected String doInBackground(Void... voids) {
            try {
                Thread.sleep(5000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Hello from other Thread";
        }

        @Override
        protected void onPostExecute(String s) {
            if (null != textView) {
                textView.setText(s);
            }
        }


        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }
    }
}
