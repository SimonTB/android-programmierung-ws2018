package de.simontb.sportkurse.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.simontb.sportkurse.R;
import de.simontb.sportkurse.model.Course;

public class CourseListAdapter extends ArrayAdapter<Course> {



    public CourseListAdapter(Context context, List<Course> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
    }

    @Override
    public View getView(int position,
                        View convertView,
                        ViewGroup parent) {
        View rowView = convertView;
        if (null == rowView) {
            LayoutInflater inflater = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
            rowView = inflater.inflate(R.layout.row_course, parent, false);
            ImageView courseIconView = rowView.findViewById(R.id.course_icon);
            TextView courseNameTextView = rowView.findViewById(R.id.course_name);
            CourseViewHolder viewHolder = new CourseViewHolder();
            viewHolder.courseIconView = courseIconView;
            viewHolder.courseNameView = courseNameTextView;
            rowView.setTag(viewHolder);
        }
        Course course = getItem(position);
        CourseViewHolder viewHolder = (CourseViewHolder) rowView.getTag();
        viewHolder.courseNameView.setText(course.getName());
        viewHolder.courseIconView.setImageResource(R.drawable.ic_directions);
        return rowView;
    }

    static class CourseViewHolder{
        ImageView courseIconView;
        TextView courseNameView;
    }

}
