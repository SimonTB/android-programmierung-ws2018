package de.simontb.sportkurse;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import de.simontb.sportkurse.database.CourseDatabaseOpenHelper;

public class CourseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SQLiteDatabase db = new CourseDatabaseOpenHelper(this).getWritableDatabase();
        final Cursor courses = db.query("courses", new String[]{"_id", "name"}, null, null, null, null, null);
        if (0 == courses.getCount()) {
            for (int i = 1; i <= 50; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", "Kurs " + i);
                db.insert("courses", null, contentValues);
            }
        }
        courses.close();
    }

}
