package de.simontb.sportkurse.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.simontb.sportkurse.R;

public class CourseDetailFragment extends Fragment {

    public static final String EXTRA_COURSE_NAME = "course_name";

    @BindView(R.id.course_name)
    TextView courseNameView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_course_detail, container, false);
        ButterKnife.bind(this, view);
        String courseName = getArguments().getString(EXTRA_COURSE_NAME);
        courseNameView.setText(courseName);
        return view;
    }

}
