package de.simontb.sportkurse.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import de.simontb.sportkurse.R;
import de.simontb.sportkurse.ui.fragment.CourseDetailFragment;

public class CourseDetailActivity extends AppCompatActivity {

    public static final String EXTRA_COURSE_NAME = "course_name";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);
        Fragment detailFragment = new CourseDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putString(CourseDetailFragment.EXTRA_COURSE_NAME,
                getIntent().getStringExtra(EXTRA_COURSE_NAME));
        detailFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.detailsContainer, detailFragment, "DetailFragment")
                .commit();
    }

}
