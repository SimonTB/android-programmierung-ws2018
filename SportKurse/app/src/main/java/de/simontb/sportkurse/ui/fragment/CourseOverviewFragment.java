package de.simontb.sportkurse.ui.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import de.simontb.sportkurse.R;
import de.simontb.sportkurse.database.CourseDatabaseOpenHelper;
import de.simontb.sportkurse.ui.activity.CourseDetailActivity;
import de.simontb.sportkurse.ui.activity.CourseOverviewActivity;
import de.simontb.sportkurse.ui.adapter.CourseListCursorAdapter;

public class CourseOverviewFragment extends Fragment {

    ListView courseList;
//    List<Course> courses;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_course_overview, container, false);
        courseList = view.findViewById(R.id.course_list);

        SQLiteDatabase db = new CourseDatabaseOpenHelper(getContext()).getReadableDatabase();
        final Cursor courses = db.query("courses", new String[]{"_id", "name"}, null, null, null, null, null);
        ListAdapter courseListAdapter = new CourseListCursorAdapter(getContext(), courses);
        courseList.setAdapter(courseListAdapter);

//        this.courses = new ArrayList<>(50);
//        for (int i = 1; i <= 50; i++) {
//            this.courses.add(new Course("Kurs " + i));
//        }
//        ListAdapter courseListAdapter = new CourseListAdapter(this, this.courses);
//        courseList.setAdapter(courseListAdapter);
        courseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                courses.moveToPosition(position);
                int nameColumnIndex = courses.getColumnIndex("name");
                String courseName = courses.getString(nameColumnIndex);
                CourseOverviewActivity activity = (CourseOverviewActivity) getActivity();
                activity.showCourse(courseName);
            }
        });
        return view;
    }

}
