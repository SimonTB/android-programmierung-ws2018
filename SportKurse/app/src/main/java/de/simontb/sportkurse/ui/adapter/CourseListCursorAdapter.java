package de.simontb.sportkurse.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import de.simontb.sportkurse.R;

public class CourseListCursorAdapter extends CursorAdapter {

    public CourseListCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        return inflater.inflate(R.layout.row_course, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView courseIconView = view.findViewById(R.id.course_icon);
        TextView courseNameTextView = view.findViewById(R.id.course_name);
        courseIconView.setImageResource(R.drawable.ic_directions);
        String courseName = cursor.getString(1);
        courseNameTextView.setText(courseName);
    }

}
