package de.simontb.sportkurse.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import de.simontb.sportkurse.R;
import de.simontb.sportkurse.ui.fragment.CourseDetailFragment;

public class CourseOverviewActivity extends AppCompatActivity {

    View detailsContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_overview);
        detailsContainer = findViewById(R.id.detailsContainer);
    }

    public void showCourse(String courseName) {
        if (null == detailsContainer) {
            Intent detailIntent = new Intent(this, CourseDetailActivity.class);
            detailIntent.putExtra(CourseDetailActivity.EXTRA_COURSE_NAME, courseName);
            startActivity(detailIntent);
        } else {
            Fragment detailFragment = new CourseDetailFragment();
            Bundle arguments = new Bundle();
            arguments.putString(CourseDetailFragment.EXTRA_COURSE_NAME, courseName);
            detailFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detailsContainer, detailFragment, "DetailFragment")
                    .commit();
        }
    }

}
