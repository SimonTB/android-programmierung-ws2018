package de.simontb.sportkurse.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.test.RenamingDelegatingContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CourseDatabaseTest {

    private SQLiteDatabase db;

    @Before
    public void setUp() throws Exception {
        Context context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "test_");
        CourseDatabaseOpenHelper openHelper = new CourseDatabaseOpenHelper(context);
        db = openHelper.getWritableDatabase();
        db.delete("courses", null, null);
        db.execSQL("INSERT INTO courses VALUES(1, 'Kurs 1')");
        db.execSQL("INSERT INTO courses VALUES(2, 'Kurs 2')");
        db.execSQL("INSERT INTO courses VALUES(3, 'Kurs 3')");
        db.execSQL("INSERT INTO courses VALUES(4, 'Kurs 4')");
        db.execSQL("INSERT INTO courses VALUES(5, 'Kurs 5')");
    }

    @Test
    public void testReadFromDb() {
        Cursor cursor = db.query("courses", new String[]{"name"}, null, null, null, null, null);

        assertNotNull(cursor);
        assertEquals(5, cursor.getCount());
    }

    @Test
    public void testReadSingleCourse() {
        Cursor cursor = db.query("courses", new String[]{"name"}, "_id=?", new String[]{"1"},null, null, null);


        assertNotNull(cursor);
        assertEquals(1, cursor.getCount());
        assertTrue(cursor.moveToFirst());
        assertEquals("Kurs 1",cursor.getString(cursor.getColumnIndexOrThrow("name")));
    }

    @Test
    public void testInsertNewCourse() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "Kurs 6");

        db.insert("courses", null, contentValues);

        Cursor cursor = db.query("courses", new String[]{"name"}, null, null, null, null, null);
        assertNotNull(cursor);
        assertEquals(6, cursor.getCount());
    }

    @Test(expected = SQLiteConstraintException.class)
    public void testPrimaryKeyUnique() {
        db.execSQL("INSERT INTO courses VALUES(5, 'Kurs 5')");
    }

}
