package de.simontb.helloworld;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView outputView = findViewById(R.id.output);
        final EditText input = findViewById(R.id.input);
        Button okButton = findViewById(R.id.okButton);
        final TextView happyText = findViewById(R.id.happyText);
        okButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, GreetingActivity.class);
            intent.putExtra(GreetingActivity.EXTRA_NAME, input.getText().toString());
            startActivity(intent);
        });
        happyText.setVisibility(View.GONE);
    }

}
