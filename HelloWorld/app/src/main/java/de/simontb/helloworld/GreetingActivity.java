package de.simontb.helloworld;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class GreetingActivity extends Activity {

    private static final String TAG = GreetingActivity.class.getSimpleName();
    public static final String EXTRA_NAME = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_greeting);
        Intent callingIntent = getIntent();
        String name = callingIntent.getStringExtra(EXTRA_NAME);
        final TextView greetingView = findViewById(R.id.greetingView);
        String greeting = getString(R.string.hello_text, name);
        greetingView.setText(greeting);
    }

}
