package de.simontb.contacts;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int REQUEST_CODE_PICK_CONTACT = 1;
    private static final int REQUEST_CODE_CONTACTS_PERMISSION = 2;

    TextView pickedContactView;
    TextView numberOfContactsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button pickContactButton = findViewById(R.id.pickContactButton);
        pickedContactView = findViewById(R.id.textView);
        numberOfContactsView = findViewById(R.id.numberOfContacts);
        pickContactButton.setOnClickListener(view -> pickContact());
        getNumberOfContacts();
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(pickContactIntent, REQUEST_CODE_PICK_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_PICK_CONTACT:
                if (resultCode == RESULT_OK) {
                    Uri contactUri = data.getData();
                    String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
                    Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
                    cursor.moveToFirst();
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    cursor.close();
                    pickedContactView.setText("Picked: " + name);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void getNumberOfContacts() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CODE_CONTACTS_PERMISSION);
        } else {
            queryNumberOfContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_CONTACTS_PERMISSION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            queryNumberOfContacts();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void queryNumberOfContacts() {
        getSupportLoaderManager().initLoader(1, null, this);
//        String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
//        Cursor cursor = getContentResolver().query(
//                ContactsContract.Contacts.CONTENT_URI,
//                projection, null, null, null
//        );
//        int numberOfContacts = cursor.getCount();
//        cursor.close();
//        numberOfContactsView.setText("You can choose between " + numberOfContacts + " contacts");
    }

    // Nicht unbedingt eine gute Praxis für UX, dient hier lediglich als Beispiel, überlegt euch, ob sowas für euch Sinn macht
    @Override
    public void onBackPressed() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to exit the application?")
                .setPositiveButton("Yes", (dialog, which) -> super.onBackPressed())
                .setNegativeButton("No", (dialog, which) -> {
                })
                .create();
        alertDialog.show();
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
        return new CursorLoader(this,
                ContactsContract.Contacts.CONTENT_URI,
                projection,
                null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        int numberOfContacts = cursor.getCount();
        numberOfContactsView.setText("You can choose between " + numberOfContacts + " contacts");
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }
}
