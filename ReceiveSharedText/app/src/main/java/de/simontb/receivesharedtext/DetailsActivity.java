package de.simontb.receivesharedtext;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        TextView details = findViewById(R.id.textView);
        details.setText(getIntent().getStringExtra(Intent.EXTRA_TEXT));
    }
}
